#include "Sorted_List.h"
#include <iostream>

using namespace std;


Sorted_List::Sorted_List(int val) : first_link{new Node(val)}, count{1} 
{
}

Sorted_List::Sorted_List() : first_link{nullptr}, count{0} 
{
}

Sorted_List::Sorted_List(Sorted_List const& other) 
{
    count = other.count;
    first_link = other.is_empty() ? nullptr : new Node(*(other.first_link));
}
Sorted_List::Sorted_List(Sorted_List && other)
{
    count = other.count;
    first_link = other.first_link;

    other.count = 0;
    other.first_link = nullptr;
}
Sorted_List & Sorted_List::operator=(Sorted_List const& other)
{   
    clear();
    count = other.count;
    first_link = other.is_empty() ? nullptr : new Node(*(other.first_link));
    
    return *this;
}
Sorted_List & Sorted_List::operator=(Sorted_List && other)
{
    clear();

    count = other.count;
    first_link = other.first_link;

    other.count = 0;
    other.first_link = nullptr;

    return *this;
}
void Sorted_List::insert(int i) 
{
    if (count == 0) 
    {
        first_link = new Node(i);
    }
    else if (first_link->get_value() >= i)
    {   
        Node* old_first = first_link;
        first_link = new Node(i, old_first);
    }
    else 
    {
        first_link->insert(i);
    }
    
    count++;
}

void Sorted_List::remove(int i) 
{
    if (count == 0)
        return;
    
    Node* prev_node = nullptr;
    Node* iterator = first_link;
    while ( iterator != nullptr) //no garentee of unique value
    {
        if (iterator->get_value() == i) 
        {
            if(prev_node == nullptr) //if prev_node is null then we are at the first element
            {
                first_link = iterator->next();
                delete iterator;
                iterator = first_link;
            }
            else
            {
                prev_node->next(iterator->next());
                delete iterator;
                iterator = prev_node->next(); //prev_node does not change
            }
            --count;
            return; //remove this return for removing all elem with the given value in the list
        }
        else
        {
            prev_node = iterator;
            iterator = iterator->next();
        }
    }
}

Node* Sorted_List::head() const
{
    return first_link;
}

Node* Sorted_List::tail() const
{
    if( (first_link != nullptr) and 
        (first_link->has_next_elem()))
        return first_link->next();
    else
        return nullptr;
}

Node* Sorted_List::last() const
{
    if(first_link != nullptr)
        return first_link->last();
    else
        return nullptr;
}

bool Sorted_List::is_empty() const
{
    return first_link == nullptr;
}

int Sorted_List::size() const
{
    return count;
}

bool Sorted_List::contains(int i) const
{
    return first_link->contains(i);
}

void Sorted_List::print() const
{
    cout << "List : ";
    if (count) first_link->print();
    else cout << "empty" << endl;
}

void Sorted_List::clear() 
{
    if (!is_empty())
    {
        first_link->clear();
        delete first_link;
    }
    first_link = nullptr;
    count = 0;
}

bool Sorted_List::operator==(Sorted_List const& other) const
{
    return ( (*first_link) == *(other.first_link) );
}

Sorted_List::~Sorted_List()
{
    clear();
}


