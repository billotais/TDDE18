// This test program uses a test framework supporting TDD and BDD.
// You are not required to use the framework, but encouraged to.
// Code:
// https://github.com/philsquared/Catch.git
// Documentation:
// https://github.com/philsquared/Catch/blob/master/docs/tutorial.md

// You ARE however required to implement all test cases outlined here,
// even if you do it by way of your own function for each case.  You
// are recommended to solve the cases in order, and rerun all tests
// after you modify your code.

// This define lets Catch create the main test program
// (Must be in only one place!)
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Sorted_List.h"
#include <random>

//=======================================================================
// Test cases
//=======================================================================

TEST_CASE( "Create an empty list" ) 
{
  Sorted_List l{};

  REQUIRE( l.is_empty() == true );
  REQUIRE( l.size() == 0 );
}

// Solve one TEST_CASE or WHEN at a time!
//
// Move this comment and following #if 0 down one case at a time!
// Make sure to close any open braces before this comment.
// The #if 0 will disable the rest of the file.


TEST_CASE( "Insert an item in an empty list" ) 
{
  Sorted_List l{};

  l.insert(5);
  
  REQUIRE( l.is_empty() == false );
  REQUIRE( l.size() == 1 );
  
}

SCENARIO( "Empty lists" ) 
{
  
  GIVEN( "An empty list" ) 
  {
    Sorted_List l{};

    REQUIRE( l.is_empty() == true );
    REQUIRE( l.size() == 0 );
    
    WHEN( "an item is inserted" ) 
    {

      l.insert(5);
      
      THEN( "the size increase and the item is first in the list" ) 
      {
        REQUIRE( l.is_empty() == false );
        REQUIRE( l.size() == 1 );
        REQUIRE( l.head()->get_value() == 5/* test that item is first in list */ );
        REQUIRE( l.last()->get_value() == 5);
      }
    }
    
    WHEN( "an item is removed" ) 
    {
        l.remove(5);

        THEN( "the list is still empty" ) 
        {
            REQUIRE( l.is_empty() == true );
            REQUIRE( l.size() == 0 );
        }
    }
    
    WHEN( "the list is copied to a new list" ) 
    {

      // copy your list to a new variable (copy constructor)
      Sorted_List copied_l{l};
      
      THEN( "the new list is also empty" ) 
      {
        // add your REQUIRE statements
        REQUIRE( copied_l.is_empty() == true );
        REQUIRE( l.is_empty() == copied_l.is_empty() );
        REQUIRE( copied_l.size() == 0 );
        REQUIRE( l.size() == copied_l.size() );
        
        REQUIRE( l.head() == copied_l.head());
        REQUIRE( l.tail() == copied_l.tail());
        REQUIRE( l.last() == copied_l.last());
          
      }
    }
    
    WHEN( "the list is copied to an existing non-empty list" ) 
    {
      // create and fill a list to act as the existing list
      // copy (assign) your empty list to the existing
      Sorted_List filled_l{};
      REQUIRE( filled_l.is_empty() == true );
      REQUIRE( filled_l.size() == 0 );
      
      for(int i{0}; i < 100 ; ++i)
        filled_l.insert(i);
        
      REQUIRE( filled_l.is_empty() == false );
      
      filled_l = l;
      
      THEN( "the existing list is also empty" ) 
      {
        // add your REQUIRE statements
        REQUIRE( filled_l.is_empty() == true );
        REQUIRE( filled_l.size() == 0 );
      }
    }
  }
}


SCENARIO( "Single item lists" ) 
{

  GIVEN( "A list with one item in it, added after" ) 
  {
    Sorted_List l{};
    l.insert(5);
    REQUIRE( l.is_empty() == false );
    REQUIRE( l.size() == 1 );
    // create the given scenario
    
    WHEN( "a smaller item is inserted" ) 
    {
        l.insert(4);
        THEN( "4 should be at frst" ) 
        {
            REQUIRE( l.head()->get_value() == 4);
            REQUIRE( l.last()->get_value() == 5);
        }
    }
    
    WHEN( "a larger item is inserted" ) 
    {
        l.insert(6);
        THEN( "it should be at last" ) 
        {
            REQUIRE( l.head()->get_value() == 5);
            REQUIRE( l.last()->get_value() == 6);
        }
    }
    
    WHEN( "an item is removed" ) 
    {
        l.remove(5);
        THEN( "the list should be empty" ) 
        {
            REQUIRE( l.is_empty() == true );
            REQUIRE( l.size() == 0 );
            REQUIRE( l.head() == l.last());
            REQUIRE( l.tail() == nullptr );
            
        }
    }
    
    WHEN( "the list is copied to a new list" ) 
    {
      Sorted_List copied_l{l};
      
      THEN( "the new list is also a one elment list" ) 
      {
        // add your REQUIRE statements
        REQUIRE( copied_l.is_empty() == false );
        REQUIRE( copied_l.size()==1);
        REQUIRE( l.is_empty() == copied_l.is_empty() );
        REQUIRE( l.size() == copied_l.size() );
        REQUIRE( l.head()->get_value() == copied_l.head()->get_value() );
        REQUIRE( copied_l.contains(5) == true);
        REQUIRE( l.contains(5) == copied_l.contains(5));
        /*REQUIRE( l.head() == copied_l.head());
        REQUIRE( l.tail() == copied_l.tail());
        REQUIRE( l.last() == copied_l.last());*/
          
      }
    }
    
    WHEN( "the list is copied to an existing non-empty list" ) 
    {
        Sorted_List filled_l{};
        REQUIRE( filled_l.is_empty() == true );
        REQUIRE( filled_l.size() == 0 );

        for(int i{0}; i < 100 ; ++i)
            filled_l.insert(i);

        REQUIRE( filled_l.is_empty() == false );

        filled_l = l;
        THEN( "the two list contain the same value but not on the same node" ) 
        {
            REQUIRE( filled_l.is_empty() == false );
            REQUIRE( l.is_empty() == filled_l.is_empty() );
            
            REQUIRE( filled_l.size()==1);
            REQUIRE( l.size() == filled_l.size() );
            
            REQUIRE( filled_l.contains(5) == true);
            REQUIRE( l.contains(5) == filled_l.contains(5));
            
            REQUIRE( filled_l.head()->get_value() == 5 );
            REQUIRE( l.head()->get_value() == filled_l.head()->get_value() );
            REQUIRE( l.last()->get_value() == filled_l.last()->get_value() );
            
            REQUIRE( l.head() != filled_l.head()); //should hold same value but should be same pointer
            REQUIRE( l.last() != filled_l.last());//should hold same value but should be same pointer
            REQUIRE( l == filled_l); //
            REQUIRE( l.tail() == filled_l.tail());//should be both null
        }
    }
    
  }
  GIVEN( "A list with one item in it, direclty added" ) 
  {
    Sorted_List l{5};
    
    REQUIRE( l.is_empty() == false );
    REQUIRE( l.size() == 1 );
    // create the given scenario
    
    WHEN( "a smaller item is inserted" ) 
    {
        l.insert(3);
        THEN( "3 should be at frst" ) 
        {
            REQUIRE( l.head()->get_value() == 3);
            REQUIRE( l.last()->get_value() == 5);
        }
    }
    
    WHEN( "a larger item is inserted" ) 
    {
        l.insert(7);
        THEN( "it should be at last" ) 
        {
            REQUIRE( l.head()->get_value() == 5);
            REQUIRE( l.last()->get_value() == 7);
        }
    }
    
    WHEN( "an item is removed" ) 
    {
        l.remove(5);
        THEN( "the list should be empty" ) 
        {
            REQUIRE( l.is_empty() == true );
            REQUIRE( l.size() == 0 );
            REQUIRE( l.head() == l.last());
            REQUIRE( l.tail() == nullptr );
        }
    }
    
    WHEN( "the list is copied to a new list" ) 
    {
      Sorted_List copied_l{l};
     
      THEN( "the new list is also a one elment list" ) 
      {
        // add your REQUIRE statements
        REQUIRE( copied_l.is_empty() == false );
        REQUIRE( copied_l.size()==1);
        REQUIRE( l.is_empty() == copied_l.is_empty() );
        REQUIRE( l.size() == copied_l.size() );
        REQUIRE( l.head()->get_value() == copied_l.head()->get_value() );
        REQUIRE( copied_l.contains(5) == true);
        REQUIRE( l.contains(5) == copied_l.contains(5));
      }
    }
    
    WHEN( "the list is copied to an existing non-empty list" ) 
    {
        Sorted_List filled_l{};
        REQUIRE( filled_l.is_empty() == true );
        REQUIRE( filled_l.size() == 0 );

        for(int i{0}; i < 100 ; ++i)
            filled_l.insert(i);

        REQUIRE( filled_l.is_empty() == false );

        filled_l = l;
        THEN( "the two list contain the same value but not on the same node" ) 
        {
            REQUIRE( filled_l.is_empty() == false );
            REQUIRE( l.is_empty() == filled_l.is_empty() );
            
            REQUIRE( filled_l.size()==1);
            REQUIRE( l.size() == filled_l.size() );
            
            REQUIRE( filled_l.contains(5) == true);
            REQUIRE( l.contains(5) == filled_l.contains(5));
            
            REQUIRE( filled_l.head()->get_value() == 5 );
            REQUIRE( l.head()->get_value() == filled_l.head()->get_value() );
            REQUIRE( l.last()->get_value() == filled_l.last()->get_value() );
            
            REQUIRE( l.head() != filled_l.head()); //should hold same value but should be same pointer
            REQUIRE( l.last() != filled_l.last());//should hold same value but should be same pointer
            REQUIRE( l == filled_l); //
            REQUIRE( l.tail() == filled_l.tail());//should be both null
        }
    }
    
  }
}


SCENARIO( "Multi-item lists" ) 
{

  GIVEN( "A list with five items in it" ) 
  {
    Sorted_List l{};

    l.insert(0);
    
    for(int i{2}; i < 99 ; ++i)
        l.insert(i);
     
    l.insert(100);
    
    
    REQUIRE( l.size() == (100-1) ); //from 0 to 100 wihtout 1 and 99

    WHEN( "an item smaller than all other is inserted" ) 
    {
        l.insert(-1);
        THEN( "it should be at first" ) 
        {
            REQUIRE( l.head()->get_value() == -1);
            REQUIRE( l.last()->get_value() == 100);
        }
    }
    
    WHEN( "an item larger than all other is inserted" ) 
    {
        l.insert(101);
        THEN( "it should be at last" ) 
        {
            REQUIRE( l.head()->get_value() == 0);
            REQUIRE( l.last()->get_value() == 101);
        }
    }
    
    WHEN( "an item smaller than all but one item is inserted" ) 
    {
        l.insert(1);
        THEN( "it should second and not at first" ) 
        {
            REQUIRE( l.head()->get_value() == 0);
            REQUIRE( l.last()->get_value() == 100);
            REQUIRE( l.head()->next()->get_value() == 1);
        }
    }
    
    WHEN( "an item larger than all but one item is inserted" ) 
    {
        //given that it is not a linked list we should get the second element first by traversing the list
        Node* before_last = l.tail();
        while(before_last->next()->has_next_elem())
            before_last  = before_last->next();
        
        l.insert(99);
        
        THEN( "it should be second last and not at last posistion" ) 
        {
            
            REQUIRE( l.head()->get_value() == 0);
            REQUIRE( l.last()->get_value() == 100);
            REQUIRE( before_last->next()->get_value() == 99);
            //REQUIRE( 98 == 99);
        }
    }
    
    WHEN( "an item is removed" ) 
    {
        REQUIRE( l.contains(50) == true);
        l.remove(50);

        THEN( "the list should not contain it" ) 
        {
            REQUIRE( l.contains(50) == false);
        }
    }
    
    WHEN( "all items are removed" ) 
    {
        l.clear();
        THEN( "the list should be empty" ) 
        {
            REQUIRE( l.is_empty() == true);
        }
    }
    
    WHEN( "the list is copied to a new list" ) 
    {
        Sorted_List copied_l{l};
        THEN( "the lists should be equals" ) 
        {
            REQUIRE( l == copied_l);
        }
    }
    
    WHEN( "the list is copied to an existing non-empty list" ) 
    {
        Sorted_List copied_l{};
        copied_l.insert(6);
        copied_l = l;
        THEN( "the lists should be equals" ) 
        {
            REQUIRE( l == copied_l);
        }
    }
  }
  GIVEN(" A list with multiple time the same element")
  {
    Sorted_List l{};
    l.insert(2);
    l.insert(2);
    l.insert(2);
    l.insert(3);
    l.insert(3);
    l.insert(4);
    REQUIRE( l.size() == 6);
    WHEN("We remove a multiple element once")
    {
        l.remove(3);
        THEN("Only one copy of the element is removed")
        {
            REQUIRE( l.contains(3));
            REQUIRE( l.size() == 5);

        }
    }

  }
}

SCENARIO( "Lists can be copied" ) 
{

    GIVEN( "A list with five items in it, and a new copy of that list" ) 
    {
        Sorted_List l{};

        for(int i{0}; i < 5 ; ++i)
            l.insert(i);
        
        REQUIRE( l.size() == 5 );
        
        Sorted_List copied_l{l};

        WHEN( "the original list is changed" ) 
        {
            l.clear();
            
            THEN( "the copy remain unmodified" ) 
            {
                REQUIRE( l.size() != copied_l.size() );
                REQUIRE( copied_l.size() == 5 );
            }
        }

        WHEN( "the copied list is changed" ) 
        {
            copied_l.clear();
            
            THEN( "the original remain unmodified" ) 
            {
                REQUIRE( l.size() == 5 );
                REQUIRE( l.size() != copied_l.size() );
            }
        }
    }
    GIVEN( "A list with five items in it, and a new copy of that list using assignment operator" ) 
    {
        Sorted_List l{};

        for(int i{0}; i < 5 ; ++i)
            l.insert(i);
        
        REQUIRE( l.size() == 5 );
        
        Sorted_List copied_l{};
        copied_l = l;

        WHEN( "the original list is changed" ) 
        {
            l.clear();
            
            THEN( "the copy remain unmodified" ) 
            {
                REQUIRE( l.size() != copied_l.size() );
                REQUIRE( copied_l.size() == 5 );
            }
        }

        WHEN( "the copied list is changed" ) 
        {
            copied_l.clear();
            
            THEN( "the original remain unmodified" ) 
            {
                REQUIRE( l.size() == 5 );
                REQUIRE( l.size() != copied_l.size() );
            }
        }
    }
}

SCENARIO( "Lists can be heavily used" ) 
{

  GIVEN( "A list with 1000 random items in it" ) 
  {
    Sorted_List l{};
    // create the given list with 1000 random items
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> uniform(1,1000);
    
    for (int i{0}; i < 1000; ++i)
    {
      int random = uniform(gen); // generate a random number
      l.insert(random);
    }
    
    WHEN( "the list have 1000 random numbers inserted" ) 
    {
        for (int i{0}; i < 1000; ++i)
        {
            int random = uniform(gen); // generate a random number
            l.insert(random);
        }
        
        THEN ("the list have 2000 items in correct order")
        {
            REQUIRE( l.size() == 2000 );
        }
    }

    WHEN( "the list have the 1000 random numbers removed" ) 
    {
        for (int i{0}; i < 1000; ++i)
            l.remove(l.head()->get_value());
        THEN("the list is empty")
        {
            REQUIRE( l.size() == 0 );
            REQUIRE( l.is_empty() == true );
        }
    }
  }
}



Sorted_List trigger_move(Sorted_List l)
{
    // do some (any) modification to list
    
    l.remove(4);
    return l;
}

SCENARIO( "Lists can be passed to functions" ) 
{

  GIVEN( "A list with 5 items in it" ) 
  {

    Sorted_List given{};
    // insert 5 items
    for(int i{0}; i < 5 ; ++i)
        given.insert(i);
  
    WHEN( "the list is passed to trigger_move()" ) 
    {
      Sorted_List l{ trigger_move(given) };
      THEN( "the given list remain and the result have the change" ) 
      {
          REQUIRE( l.size() == 4 );
          REQUIRE( given.size() == 5 );
      }
    }
  }
}


// To really verify that your "important five" are called you should
// add cout-statements in each during development (comment out before
// demonstration).

// In addition you must of course verify that the list is printed
// correct and that no memory leaks occur during use. You can solve
// the printing part on your own. Here's how to run the (test) program
// when you check for memory leaks:
//
// $ valgrind --tool=memcheck a.out

// Finally you need to check that you can do all operations that make
// sens also on a immutable list (eg all operations but insert): 

void use_const_list(Sorted_List const& l)
{
    // We can call all const methods
    l.head();
    l.tail();
    l.last();
    l.is_empty();
    l.size(); 
    l.contains(1);
  
}
SCENARIO( "Immutable lists are correctly implemented" ) 
{

  GIVEN( "A list with 5 items in it" ) 
  {

    Sorted_List given{};
    // insert 5 items
    for(int i{0}; i < 5 ; ++i)
        given.insert(i);
  
    WHEN( "the list is passed to a const function" ) 
    {
      use_const_list(given);
      THEN( "the given list remain and the result have the change" ) 
      {
          REQUIRE( true );
      }
    }
  }
}
#if 0

#endif
