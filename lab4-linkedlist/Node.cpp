#include "Node.h"
#include <iostream>

using namespace std;

Node::Node(int val) : Node(val, nullptr) 
{
}

Node::Node(int val, Node* n) : value{val}, next_elem{n} 
{
}

Node::Node(Node const& other)
{
    value = other.value;
    next_elem = other.has_next_elem() ? new Node(*(other.next_elem)) : nullptr;
}
Node::Node(Node && other)
{
    value = other.value;
    next_elem = other.next_elem;

    other.value = 0;
    other.next_elem = nullptr;
}
Node & Node::operator=(Node const& other)
{
    clear();
    value = other.value;
    next_elem = other.has_next_elem() ? new Node(*(other.next_elem)) : nullptr;
    return *this;
}
Node & Node::operator=(Node && other)
{
    clear();
    value = other.value;
    next_elem = other.next_elem;

    other.value = 0;
    other.next_elem = nullptr;

    return *this;
}
void Node::next(Node* new_next) 
{
    next_elem = new_next;
}
void Node::insert(int i) // Recursive insertion
{   
    if (!has_next_elem()) 
    {
        Node* inserted = new Node(i);
        next(inserted);
    }
    else if (value <= i && i <= next_elem->get_value()) 
    {
        next_elem = new Node(i, next_elem);
    }
    else 
    {
        next_elem->insert(i);
    }
}
Node* Node::next() const // Get next element
{
    return next_elem;
}

Node* Node::last() // Recursive last()
{
    if (has_next_elem()) 
    {
        return next_elem->last();
    }
    else 
    {
        return this;
    }
}

bool Node::contains(int i) const // Recursize contains
{
    if (value == i) 
        return true;
    else if (has_next_elem()) 
        return next_elem->contains(i);
    else 
        return false;
}

int Node::get_value() const
{
    return value;
}
bool Node::has_next_elem() const
{
    return next_elem; //check if null
}

void Node::print() const
{
    cout << value << " ";
    if (has_next_elem()) 
        next_elem->print();
    else cout << endl;
}
void Node::clear()
{
    if (has_next_elem())
    {
        next_elem->clear();
        delete next_elem;
    }
    next_elem = nullptr;
}

bool Node::operator==(Node const& other) const
{
    if (has_next_elem())
    {
        return (value == other.value) and (*next_elem) == ( *(other.next_elem) ); //value pointed comparison
    }
    else
    {
        return (value == other.value) and next_elem == other.next_elem; //ptr comparison where we should have both at nullptr
    }
}

Node::~Node()
{
    
}

