#ifndef SORTED_LIST_H
#define SORTED_LIST_H

#include <iostream>

#include "Node.h"

#include "catch.hpp"

class Sorted_List {
public: 
    Sorted_List(int); // Construct a List with an element
    Sorted_List(); // Construct an empty list
    Sorted_List(Sorted_List const& other); // Copy constructor
    Sorted_List(Sorted_List && other); // Move constructor
    Sorted_List & operator=(Sorted_List const& other); // Copy assign. operator
    Sorted_List & operator=(Sorted_List && other); // Move assign. operator
    
    void insert(int); // Insert element
    void remove(int); // Remove 1 occurence of element
    Node* head() const; // Get pointer to first element
    Node* tail() const; // Get pointer to second element
    Node* last() const; // Get pointer to last element
    bool is_empty() const; // Is the list empty
    int size() const; // get the size of the list
    bool contains(int) const; // Check if the element is in the list
    void print() const; // Print the list
    void clear(); // Delete all data of the list
    bool operator==(Sorted_List const&) const; // == operator, compare values of 2 lists
    ~Sorted_List(); // Destructor

private:
    Node* first_link{nullptr}; // Point to first element of the list
    int count{0}; // Number of elements in the list
    

};

#endif
