#ifndef NODE_H
#define NODE_H

class Node {
    public:

        Node(int); // Create node with value and no successor
        Node(int, Node*); // Create node with value and succesor node
        Node(Node const& other); // Copy constructor
        Node(Node && other); // Move constructor
        Node & operator=(Node const& other); // Copy assign. operator
        Node & operator=(Node && other); // Move assign. operator
        void next(Node*); // Change the successor of the node
        void insert(int); // Helper method for recursive insert(int)
        Node* next() const; // Get pointer to next node
        Node* last(); // Helper method for recursive last()
        bool contains(int) const; // Helper method for recursive contains()
        int get_value() const; // Return value stored in the node
        bool has_next_elem() const; // Check if there is a succesor node
        void print() const; // Helper method for recursive print()
        void clear(); // Helper methode for recursive clear()
        bool operator==(Node const& other) const; // == operator
        ~Node(); // Destrcutor
        
    private:
        int value; // Stored value
        Node* next_elem = nullptr; // Pointer to successor node
};

#endif
