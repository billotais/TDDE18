#include <iostream>
#include <iomanip>

using namespace std;

int main() {


    // To avoid copy-pasting "do-while" 3 times we can use a function. 
    // The version shown here does not contain said function because it was the version shown to the examiner  
    
    // Declare constants
    const float MIN_STRIDE{0.01};

    // Declare variables used to get the input
    float first_price{0.0};
    float last_price{0.0};
    float stride{0.0};
    float tax_percent{0.0};

    cout << "INPUT PART" << endl;
    cout << "==========" << endl;

    // First price input
    do {
        cout << "Enter first price: ";
        cin  >> first_price;
        if (first_price < 0) {
            cerr << "ERROR: The price must be at least 0 (zero) SEK" << endl;
        }
    } while (first_price < 0);
    
    // Last price input
    do {
        cout << "Enter last price: ";
        cin  >> last_price;

        if (last_price < first_price) {
            cerr << "ERROR: The last price can't be smaller than the first price" << endl;
        }
    } while (last_price < first_price);
    
    // Stride input
    do {
        cout << "Enter stride: ";
        cin  >> stride;

        if (stride < MIN_STRIDE) {
            cerr << "ERROR: The stride must be at least " << MIN_STRIDE << endl;
        }
    } while (stride < MIN_STRIDE);
    
    // Tax percent input
    do {
        cout << "Enter tax percent: ";
        cin  >> tax_percent;

        if (tax_percent < 0 || tax_percent > 100) {
            cerr << "ERROR: Please enter a tax percent betweeen 0%" << " and 100%" << endl;
        }
    } while (tax_percent < 0 || tax_percent > 100);
    
    cout << endl;

    // Output
    cout << "TAX TABLE" << endl;
    cout << "=========" << endl;
    cout << "       Price            Tax    Price with tax" << endl;
    cout << "---------------------------------------------" << endl;

    for (float count{first_price}; count <= last_price; count += stride) {

        float tax{count * tax_percent / 100};
        cout << fixed;
        cout << setprecision(2);
        cout << setw(12) << count;
        cout << setw(15) << tax;
        cout << setw(18) << (count + tax) << endl; 
    }
    cout << "---------------------------------------------" << endl;
    return 0;
}