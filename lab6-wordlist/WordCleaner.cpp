#include "WordCleaner.h"

using namespace std;

// Complementary work: You will need to change the regex-expression in addWord(). Im aware that its says its included
// as a volountary part of the lab in the assignment but im informed this has been announced, perhaps after you completed it.
// TL/DR: No regex.

WordCleaner::WordCleaner()
{

}

void WordCleaner::addWord(string word)
{
        word.erase(0, word.find_first_not_of("\t\n\v\f\r") ); //remove space tab and so on from start
        word.erase(0, word.find_first_not_of("\"\'(") ); //remove illegal char from start
        word.erase(word.find_last_not_of("\t\n\v\f\r") + 1); //remove space tab and so on from end
        word.erase(word.find_last_not_of("!?;,:.\"\')") + 1); //remove illegal char from end

        if( ( word.size() >= 2 ) and ( 0 == word.compare(word.size()-2, 2, "\'s") ) ) // Remove 's at the end
            word.erase(word.size()-2);

        // We now have a cleaned word, need to chck if valid
        if (word.size() < 3)
            return; // Don't accept too small words
        if (word.at(0) == '-' || word.at(word.size()-1) == '-')
            return; // Check that the word doesn't start with a '-'
        if (word.find("--") != string::npos)
            return; // Check that we don't have '--' in the word
        if (word.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-") != string::npos)
            return; // Check that the word doens't contains anythong other that chars and hyphens

        transform(word.begin(),word.end(),word.begin(),::tolower);

        words.push_back(word);
        if(word.size()>longest_word_size) longest_word_size = word.size(); // Update longest_size
}

unsigned int WordCleaner::getLongestWordSize()
{
    return longest_word_size;
}

void WordCleaner::printWithFct(function<void (pair<string const&,int> const&)> const& printFunction)
{
    for_each(sorted_valid.begin(),sorted_valid.end(),printFunction);
}

void WordCleaner::printInOrder(unsigned int size_limit)
{
    unsigned int current_size{0};
    for(string word : words)
    {
        if((current_size + word.size() + 1) < size_limit) //+1 for taking into account the psace
        {
            current_size += word.size();
            cout << word << " ";
        }
        else
        {
            current_size = word.size();
            cout << endl;
            cout << word << " ";
        }
    }
    cout << endl;
}

// Comment: I feel a little bad about giving complementary work for reasons you most likely didnt know of, so ill just
// leave this as a comment. Im sure you would have solved if u knew.
// We would like you to only avoid excessive use of for_each. For printing is fine though.
void WordCleaner::sort(function< bool ( pair<string const&,int> const& left, pair<string const&,int> const& right ) > const& comparator)
{
    map< string,int > sorted_and_unified_valid{};
    // Create a map associating each word with the number of occurences
    // Here we demonstrate how we can use a for_Each instead of a foreach with abriged syntax for(string s : collection)
    //for(string word : words) {...} // Here is the syntax with the sort foreach
    for_each (words.begin(), words.end(), // Here the syntax with the full foreach
    [&](string word)
    {
        if(sorted_and_unified_valid.count(word)>0)
        {
            sorted_and_unified_valid[word]=sorted_and_unified_valid[word]+1;
        }
        else
        {
            sorted_and_unified_valid.emplace(word,1);
        }
    });
    // Transform the map into a list of pairs so that we can sort it as we want
    for(pair<string,int> p : sorted_and_unified_valid)
    {
        sorted_valid.push_front(p);
    }

    // SOrt the list of pairs using the provided comparator
    sorted_valid.sort(comparator);
}
