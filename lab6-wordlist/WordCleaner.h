#ifndef WORDCLEANER_H
#define WORDCLEANER_H

#include <algorithm>
#include <string>
#include <list>
#include <map>
#include <regex>
#include <functional>
#include <iostream>
class WordCleaner
{
    private :
        std::list<std::string> words{};
        std::list< std::pair<std::string,int> > sorted_valid{};
        unsigned int longest_word_size{0};
        
    public :
        WordCleaner();
        void addWord(std::string word);
        void printWithFct(std::function<void (std::pair<std::string const&,int> const&)> const& printFunction);
        void sort(std::function<bool (std::pair<std::string const&,int> const&, std::pair<std::string const&,int> const&)> const& comparator);
        void printInOrder(unsigned int size_limit);
        unsigned int getLongestWordSize();
};

#endif
