#include "WordCleaner.h"
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;


void show_usage(char* program_name)
{
    std::cout << "Word List : Help :" << std::endl;
    std::cout << "  Usage: "<< program_name << " FILE [-a] [-f] [-o N]"<< std::endl;
    std::cout << "  Example: " << program_name << " example.html -a"<< std::endl;
}

bool open_file(const string &path,ifstream& file_Stream)
{
    file_Stream.open(path);
    if(!(file_Stream))
    {   
        cerr << "Error: " << path << " may not exist." << endl;
        return false;
    }
    
    return true;
}


int main(int argc, char *argv[]) 
{
    ifstream file_Stream{};
    if(argc < 3)
    {
        show_usage(argv[0]);
        return EXIT_SUCCESS;
    }
    
    if( !open_file(argv[1],file_Stream) ) // if the file is not correctly openned we exit
        return EXIT_FAILURE;
    
    if(
        argv[2][0] != '-' 
        or (argv[2][1] != 'a' and argv[2][1] != 'f' and argv[2][1] != 'o')
      )
    {
        cerr << "Error: Second argument missing or invalid.";
        return EXIT_FAILURE;
    }

    WordCleaner cleaner{};
    string word{};
    
    while(file_Stream >> word) // Read words from the file while we can
        cleaner.addWord(word);
   
    int return_code{};
    
    if ( file_Stream.eof() )
    {
        return_code = EXIT_SUCCESS;
    }
    else if ( file_Stream.bad() ) 
    {
        cerr << "Error while reading file (Bad error)" << endl;
        return_code = EXIT_FAILURE;
    }
    else if ( file_Stream.fail() )
    {
         cerr << "Logic error." << endl;
         return_code = EXIT_FAILURE;
    }
    
    file_Stream.close();

    //now that we have closed the file lets work on the rest
    
    // defining comparators

    // Compare pairs by frequency
    auto frequencyComparator = [](pair<string,int> left, pair<string,int> right)
    { 
       return left.second >= right.second; //decreaing freq
    };
    
    // Compare pairs by alphavetical order
    auto alphabeticalComparator = [](pair<string,int> left, pair<string,int> right)
    { 
        return left.first.compare(right.first) < 0; 
    };

    unsigned int width{cleaner.getLongestWordSize()};

    // Define the print functions
    auto alphaPrinter = [width](pair<string,int> toPrint)
    { 
        cout << std::left << setw(width) << toPrint.first << " " << toPrint.second << endl;
    };
    
    auto frequencyPrinter = [width](pair<string,int> toPrint)
    { 
        cout << std::right << setw(width) << toPrint.first << " " << toPrint.second << endl;
    };
    
    
    switch(argv[2][1])
    {
        case 'a':
            cleaner.sort(alphabeticalComparator);
            cleaner.printWithFct(alphaPrinter);
            break;
        case 'f':
            cleaner.sort(frequencyComparator);
            cleaner.printWithFct(frequencyPrinter);
            break;
        case 'o':
            cleaner.printInOrder(stoi(argv[3]));
            break;
    }
    
    return return_code;
}
