#include <iostream>
#include <iomanip>
using namespace std;

int main() 
{
    int x{};
    double d{};
    char c{};
    string s{};

    cout << "Enter one integer: ";
    cin  >> x;
    cout << "You entered the number: " << x << endl << endl;
    cin.ignore(256, '\n');
    
    cout << "Enter four integers: ";
    cin  >> x;
    cout << "You entered the numbers: " << x << " ";
    cin  >> x;
    cout << x << " ";
    cin  >> x;
    cout << x << " ";
    cin  >> x;
    cout << x << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter one integer and one real number:  ";
    cin  >> x >> d;
    cout << "The real is: " << setw(20) << setprecision(5) << d << endl;
    cout << "The integer is: " << setw(17) << x << endl << endl;
    cin.ignore(256, '\n');


    cout << "Enter one real and one integer number:  ";
    cin  >> d >> x;
    cout << "The real is: " << setw(20) << setfill('.') << setprecision(5) << d << endl;
    cout << "The integer is: " << setw(17) << setfill('.') << x << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter a character: ";
    cin  >> c;
    cout << "You entered: " << c << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter a word: ";
    cin  >> s;
    cout << "The word '" << s << "' has "<< s.size() << " character(s)" << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter an integer and a word: ";
    cin  >> x >> s;
    cout << "You entered '" << x << "' and \"" << s << "\"" << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter a character and a word: ";
    cin  >> c >> s;
    cout << "You entered the string \"" << s << "\" and the character '" << c << "'" << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter a word and a real number: ";
    cin  >> s >> d;
    cout << "You entered  \"" << s << "\" and '" << d << "'" << endl << endl;
    cin.ignore(256, '\n');

    cout << "Enter a text line: ";
    getline(cin, s);
    cout << "You entered \"" << s << "\"" << endl << endl;

    cout << "Enter a second line of text: ";
    getline(cin, s);
    cout << "You entered \"" << s << "\"" << endl << endl;
    
    cout << "Enter three words: ";
    cin  >> s;
    cout << "You entered: '" << s << " ";
    cin  >> s;
    cout << s << " ";
    cin  >> s;
    cout << s << "'" << endl;
    cin.ignore(256, '\n');

    return 0;
}