#include <iostream>

using namespace std;

class Logfilename
{
public:
  Logfilename() : pfix(), sfix() {}

  string name() const
  {
    return "\"" + pfix + '.' + sfix + "\"";
  }

  string name(int n) const
  {
    return "\"" + pfix + to_string(n) + '.' + sfix + "\"";
  }

  void print_rotate_command(int n)
  {
    if ( n > 1 )
    {
      cout << "mv " << name(n-1) << " " << name(n) << endl;
      print_rotate_command(n-1);
    }
    else
    {
      cout << "mv " << name() << " " << name(n) << endl;
    }
  }

  friend istream& operator>>(istream& is, Logfilename& f);

private:
  string pfix;
  string sfix;
};

istream& operator>>(istream& is, Logfilename& f)
{
  getline(is, f.pfix, '.');
  return cin >> f.sfix;
}

int main()
{
  Logfilename f;
  cout << "What is the file name? ";
  cin >> f;

  int n;
  cout << "What is the new highest numbered file? ";
  cin >> n;

  cout << "The following commands will rotate the files: " << endl;

  f.print_rotate_command(n);

  return 0;
}
