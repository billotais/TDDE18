---
title : "Exercise 2 - C++"
author : Loïs Bilat
date : 02-10-2017
...

# Exercises 2
Implement a Stack

Main file
```c++
#include <iostream>
#include "Stack.h"
using namespace std;
int main() 
{
    int x{};
    Stack s{};
    while(cin >> x) 
    {
        s.push(x);
    }
    while(!s.empty()) 
    {
        cout << top() << emdl;
        s.pop();
    }
    
    return 0;
}
```
We want to implement ``push(x)``, ``pop()``, ``top()``, ``empty()``.

```c++
void push(int const);
int top() const; // Doesn't change the underlying sata structure
void pop();
bool empty() const;
```

Header file `Stack.h`
```c++
class Stack 
{
    public:
        Stack();
        Stack(Stack const& other) // Copy constructor
        void push(int const x);
        int top() const;
        void pop();
        bool empty() const;
        
        ~Stack();
    private:
        struct Node; // Define the structure Node
        {
            int value{};
            Node* next{};
        }
        Node* copy(Node const * const node) const; // Constant pointer to a constant node
        Node* head;
}
```
The head point to the most recently added element. We will use nodes to create the structure.
Implementation file ``Stack.cpp``
````c++

Stack::Stack() : head{nullptr}
{
}
Stack::Stack(Stack const& other) : head{copy(other->head)}
{  
}
void Stack::push(int const value)
{
    Node* tmp{new Node}; // Allocate memory for a new node
    tmp->value = value;
    tmp->next = head;
    head = tmp;
}
int Stack::top() const
{
    if (empty()) throw "Error empty stack";
    return head->value;    
}
void Stack::pop() 
{
    if (empty()) throw "Error empty stack";
    Node* tmp{head};
    head = head->next;
    delete tmp; // Deallocate the memory 
}
bool Stack::empty() const
{
    return head == nullptr;
}
Node* Stack::copy(Node const * copnst node) const // Recursivly copy all the nodes
{
    if (node == nullptr)
    {
        return nullptr;
    }
    return new Node{node->value, copy(node->next)};
}
Stack::~Stack()
{
    while(!empty())
    {
        pop();
    }
}
````

