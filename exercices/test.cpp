
using namespace std;

#include <string>
#include <iostream>
#include <vector>

struct Person {
    string first_name;
    string last_name;
    int age;
};

int main() {
    string s("Hello World");
     
    for (char &c : s)
    {
        c = toupper(c);
    }
    cout << s << endl;

    vector<int> v{2, 3, 4};
    for (int &i : v)
    {
        i++;
    }
    for (int i : v)
    {
        cout << i << " ";
    }

    
}