---
title : "Exercise 1 - C++"
author : Loïs Bilat
date : 25-09-2017
...

# Exercice 1
## Read 5 numbers from a file

We start by includes and namesapce 

```c++
#include <iostream>
#include <fstream>

using namespace std;
```

We can create a function read_five_numbers

```c++
void read_five_numbers(string filename) 
{
    ifstream ifs{filename}; // Try to open the file
    if (!ifs) { // if we couldn't open
        cerr << "No file" << endl; // Error
        return; // Exit the function
    }
```

We take the name file as a parameter. We try to open it using `ifstream ifs{filename}`. We then check if we could open the file, if we couldn't, errpr
```c++
while(!ifs.eof()){
        int temp{};
        ifs >> temp;
        cout << temp; 
    } 
```
This doesn't work since eof() is not updated after reading the last number.
```c++
while(!ifs.eof()){
    int temp{};
    ifs >> temp;
    if (!ifs.eof()){
        cout << temp; 
    }
}
```
Not clean, doesn't work with characters
```c++
int temp{};
while(ifs >> temp) {
    cout << temp;
}
```
Best solution !

## Get choice from user
We have the following code
```c++
#include <iosstream>
using namespace std;

int main() {
    int choice{};
    do {
        choice = get_choice();
    } while (choice != 0);
}
```
We want to handle all possible inputs the the user enters (characters, eof(), ...).

We can start by defining the function get_choice().
```c++
int get_choice() {
    int return_value{};
    while (!(cin >> return_value)) {
        if(cin.bad()) {
            cerr << "Hardware failure";
        }
        else if(cin.fail()) {
            cerr << "Logical error";
        }
        else if (cin.eof()) {
            cerr << "End of file";
            return;
        }
    }
    return return_value;
}
```

The three flags are ``eof()``, ``fail()``, ``bad()``.

If we have reached the end of file, the loop will continue infinitly, because the condition will always be false. We need to exit the function at this moment.

If we want to clear the buffer until the next line, we use

````c++
cin.ignore(1024, '\n');
````
If the input is "a 10", we will want to ignore the a. For that, we can do 

````c++
else if(cin.fail()) {
    cerr << "Logical error";
    cin.clear();
    cin.ignore(1024, ' ');
    
}
````
We still need to reset the flag if we don't want an inifite loop. We use ``cin.clear()``.

We now need to handle cin.bad(), we need to throw an exception. 
We can write
````c++
if(cin.bad()) {
    cerr << "Hardware failure";
    throw ios::failure("bad stream");
}
````
``ios::failure`` is the type of error correspondong of a failure for a stream. "Bad stream" is the message sent to the user. Most of the time, the programm will stop when it sees this kind of error.

## Command line arguments

````
a.out testcases.txt
````
We want to get "testcases.txt" in the programm. We can use the argument counter `argc` and the argument vector ``argv``.
In this exemple, ``argc = 2``, and the vector is 
```c++
argv[0] == "a.out"
argv[1] == "testcases.txt"
```

We can write the following code
````c++
int main (int argc, char* argv[]) {
    if (argc < 2) return 1;
    ifstream ifs{argv[1]};
}
````

