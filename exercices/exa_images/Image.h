#ifndef _IMAGE_H_
#define _IMAGE_H_

class Image
{
    public:
        Image(int h, int w) : height{h}, width{w}{}
        virtual double get_size() = 0;
    protected:
        int height;
        int width;
};

class JPG : public Image {
    public:
        JPG(int h, int w) : Image(h, w){}
        double get_size();
};
class PNG : public Image {
    public:
        PNG(int h, int w) : Image(h, w){}
        double get_size();
};
class BMP : public Image {
    public:
        BMP(int h, int w) : Image(h, w){}
        double get_size();
};
#endif
