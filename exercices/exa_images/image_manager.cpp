#include <iostream>
#include <vector>
#include <string>
#include "Image.h"
using namespace std;



int main()
{
    string format{};
    int height{};
    int width{};
    vector<Image*> v{};
    do
    {
        cin >> format;
        if (format == "q") break;
        cin >> width;
        cin >> height;

        if (format == "PNG") {
            v.push_back(new PNG{height, width});

        }
        else if (format == "JPG") {
            v.push_back(new JPG{height, width});

        }
        else if (format == "BMP") {
            v.push_back(new BMP{height, width});
        }
        else {
            cout << format << " is an unsupported format !" << endl;
        }
    } while (true);
    double total{};
    for (Image* img : v)
    {
        total += img->get_size();
        delete img;
    }
    cout << "total size : " << total << endl;
    return 0;
}
