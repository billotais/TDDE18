#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <utility>

using namespace std;



int main(int argc, char* argv[])
{
    if (argc != 3) {
        cout << "use : ./elevator log.txt threshold" << endl;
        return -1;
    }
    map<string, int> usage{};
    ifstream ifs{argv[1]};
    if (!ifs) {
        cerr << "file not valid" << endl;
    }
    string line{};
    while (getline(ifs, line))
    {
        string elevator{};
        int from{};
        int to{};
        istringstream iss{line};

        iss >> elevator;
        iss >> from;
        iss >> to;
        if (!iss) {
            cout << "error in file" << endl;
            return -1;
        }

        if (usage.count(elevator))
        {
            usage[elevator] += abs(from - to);
        }
        else
        {
            usage.insert({elevator, abs(from - to)});
        }

    }

    for (pair<string, int> p : usage)
    {

        if (p.second > stoi(argv[2]))
        {
            cout << p.first << " need service" << endl;
        }
    }
    ifs.close();
    return 0;
}
