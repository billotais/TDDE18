#ifndef AABB_H
#define AABB_H

#include <iostream>
#include <iomanip>
#include <utility>
#include <algorithm>
#include <cmath>
#include <functional>
#include "Point.h"

using namespace std;

class AABB
{
    public :
        AABB(int,int,int,int);
        AABB(Point,Point);
        AABB(const AABB&); //copy ctor
        bool contain(int,int);
        bool contain(Point);
        bool intersect(AABB);
        AABB min_bounding_box(AABB);
        bool may_collide(AABB,AABB,std::function<void (int,int,int,int)>);
        bool collision_point(AABB,AABB,Point &collisionPoint,std::function<void (int,int,int,int)>);
        
        
        ~AABB();
    private :
        AABB(); //empty ctor
        int top; //y axis
        int left; //x axis
        int bottom; //y axis
        int right;//x axis
        
};


#endif
