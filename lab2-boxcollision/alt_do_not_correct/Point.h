#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <iomanip>

using namespace std;

class Point
{
    private :
        int x;
        int y;
        
    public :
        Point();
        Point(int,int);
        int getX();
        int getY();
        void setX(int);
        void setY(int); 
};


#endif
