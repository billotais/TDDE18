#include "AABB.h"

AABB::AABB() 
{
    left = 0;
    top = 0;
    bottom = 0;
    right = 0;
}

AABB::AABB(AABB const& tocopy) 
{
    left = tocopy.left;
    top = tocopy.top;
    right = tocopy.right;
    bottom = tocopy.bottom;
}


AABB::AABB(int _top,int _left, int _bottom, int _right) 
{
    // handling the user error
    
    //handling neg value
    if (_top < 0)
        _top = 0;
    
    if (_bottom < 0)
        _bottom = 0;
    
    if (_left < 0)
        _left = 0;
    
    if (_right < 0)
        _right = 0;
    
    
    //handling inverted value
    //bottom and top on the Y axis top should be above bottom. 
    //Y axis go from 0 to inf by going "down" 
    //therefor bottom should be superior or equal to top
    if(_bottom < _top) 
        swap(_bottom, _top);
    
    //the X axis is setup as in math classical view 
    //so the left side should be left of the right side
    //therefore the value of the left side should less than the value of the right side
    if(_left > _right)
        swap(_left, _right);
    
    
    //assigning the value of the box
    left = _left;
    right = _right;
    top = _top;
    bottom = _bottom;
}

AABB::AABB(Point upLeftPoint,Point bottomRightPoint) 
{
    AABB{upLeftPoint.getY(), upLeftPoint.getX(), 
         bottomRightPoint.getY(), bottomRightPoint.getX()};
}

AABB::~AABB() {}

bool AABB::contain(int x,int y) 
{
    return ( 
        ( top < y) && (y <= bottom) && 
        (left < x) && (x <= right )
    );
}

bool AABB::contain(Point p) 
{
    return contain(p.getX(), p.getY());
}


bool AABB::intersect(AABB alienBox) 
{
    /*
    for(int i{alienBox.top};i <=alienBox.bottom; ++i)
    {
        if(contain(alienBox.left,i))
        {
            return true;
        }
        if(contain(alienBox.right,i))
        {
            return true;
        }
    }
    
    for(int i{alienBox.left};i <=alienBox.right; ++i)
    {
        if(contain(i,alienBox.top))
        {
            return true;
        }
        if(contain(i,alienBox.bottom))
        {
            return true;
        }
    }
    
    for(int i{top};i <=bottom; ++i)
    {
        if(alienBox.contain(left,i))
        {
            return true;
        }
        if(alienBox.contain(right,i))
        {
            return true;
        }
    }
    
    for(int i{left};i <=right; ++i)
    {
        if(alienBox.contain(i,top))
        {
            return true;
        }
        if(alienBox.contain(i,bottom))
        {
            return true;
        }
    }
    
    return false;*/
    return  alienBox.contain(left,top) ||
            alienBox.contain(right,top)  ||
            alienBox.contain(left,bottom)  ||
            alienBox.contain(right,bottom)  ||
            contain(alienBox.left,alienBox.top) ||
            contain(alienBox.right,alienBox.top) ||
            contain(alienBox.left,alienBox.bottom) ||
            contain(alienBox.right,alienBox.bottom) ||
            (top >= alienBox.top && bottom <= alienBox.bottom && left <= alienBox.left && right >= alienBox.right) ||
            (top <= alienBox.top && bottom >= alienBox.bottom && left >= alienBox.left && right <= alienBox.right) ;
}

AABB AABB::min_bounding_box(AABB alienBox) 
{
        return AABB{min(top,alienBox.top),min(left,alienBox.left),
                    max(bottom,alienBox.bottom),max(right,alienBox.right)};
}


bool AABB::may_collide(AABB from,AABB to,std::function<void (int,int,int,int)> func) 
{
    AABB tempBox = from.min_bounding_box(to);
    func(tempBox.left,tempBox.top,tempBox.right-tempBox.left+1,tempBox.bottom-tempBox.top+1);
    return intersect(tempBox);
}



bool AABB::collision_point(AABB from,AABB to,Point &collision_point,std::function<void (int,int,int,int)> func) 
{
    double Dy{static_cast<double>(to.top - from.top)};
    double Dx{static_cast<double>(to.left - from.left)};
    
    double translated_top{static_cast<double>(from.top)};
    double translated_left{static_cast<double>(from.left)};
    
    double translated_bottom{static_cast<double>(from.bottom)};
    double translated_right{static_cast<double>(from.right)};
    
    double dx{Dx/max(abs(Dx),abs(Dy))};
    double dy{Dy/max(abs(Dx),abs(Dy))};
    
    
    bool collided{false};
    for (int i{0}; i < max(abs(Dx),abs(Dy)) && !collided; ++i)
    {
        
        translated_top += dy;
        translated_bottom += dy;
        translated_left += dx;
        translated_right += dx;
        
        
        from.top = ceil(translated_top);
        from.bottom = ceil(translated_bottom);
        from.right = ceil(translated_right);
        from.left = ceil(translated_left);
        
        func(from.left,from.top,from.right-from.left+1,from.bottom-from.top+1);
    
        collided = intersect(from); 
    }
        
    if (collided) 
    {    
        collision_point.setX(left);
        collision_point.setY(top);
    }
    return collided;
}
