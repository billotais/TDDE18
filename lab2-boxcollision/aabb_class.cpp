#include "aabb_class.h"

using namespace std;
// Complementary work:
//
// Use member initializing list instead. Initialize with braces.
// AABB::AABB() : left{_left}, top{_top}, .. ( any code to run as normal;);
// Apply for both constructors.
// Tip: If you set default values for your AABB(int, int, int, int)
// you could remove the code for this one.

// 
AABB::AABB() : top{0},left{0}, bottom{0}, right{0} 
{
}

AABB::AABB(AABB const& tocopy) 
{
    left = tocopy.left;
    top = tocopy.top;
    right = tocopy.right;
    bottom = tocopy.bottom;
}


AABB::AABB(int _top=0,int _left=0, int _bottom=0, int _right=0)
{
    // handling the user error
    
    // handling neg value
    if (_top < 0)
        _top=0;
    
    if (_bottom < 0)
        _bottom=0;
    
    if (_left < 0)
        _left=0;
    
    if (_right < 0)
        _right=0;
    
    
    //handling inverted value
    //bottom and top on the Y axis top should be above bottom. 
    //Y axis go from 0 to inf by going "down" 
    //therefor bottom should be superior or equal to top
    if(_bottom < _top) 
        swap(_bottom, _top);
    
    //the X axis is setup as in math classical view 
    //so the left side should be left of the right side
    //therefore the value of the left side should less than the value of the right side
    if(_left > _right)
        swap(_left, _right);
    
    
    //assigning the value of the box
    left=_left;
    right=_right;
    top=_top;
    bottom=_bottom;
}

AABB::AABB(Point const& upLeftPoint,Point const& bottomRightPoint)
{
    AABB{upLeftPoint.y, upLeftPoint.x,bottomRightPoint.y, bottomRightPoint.x};
}

AABB::~AABB() {}

bool AABB::contain(int x,int y) const
{
    return ( 
        ( top < y) && (y <= bottom) && 
        (left < x) && (x <= right )
    );
}

bool AABB::contain(Point const& p) const
{
    return contain(p.x, p.y);
}

// Comment: Could perhaps be simplified :-)
// Complementary work: 5-4. Applies for whole program.
bool AABB::intersect(AABB const& alienBox) const
{
    
    return  alienBox.contain(left,top) ||
            alienBox.contain(right,top)  ||
            alienBox.contain(left,bottom)  ||
            alienBox.contain(right,bottom)  ||
            contain(alienBox.left,alienBox.top) ||
            contain(alienBox.right,alienBox.top) ||
            contain(alienBox.left,alienBox.bottom) ||
            contain(alienBox.right,alienBox.bottom) ||
            (top >= alienBox.top && bottom <= alienBox.bottom && left <= alienBox.left && right >= alienBox.right) ||
            (top <= alienBox.top && bottom >= alienBox.bottom && left >= alienBox.left && right <= alienBox.right) ;
}

AABB AABB::min_bounding_box(AABB const& alienBox) const
{
        return AABB{min(top,alienBox.top),min(left,alienBox.left),
                    max(bottom,alienBox.bottom),max(right,alienBox.right)};
}


bool AABB::may_collide(AABB const& from,AABB const& to) const
{
    AABB tempBox = from.min_bounding_box(to);
    
    return intersect(tempBox);
}



bool AABB::collision_point(AABB from,AABB const& to,Point &collision_point)
{
    double Dy{static_cast<double>(to.top - from.top)};
    double Dx{static_cast<double>(to.left - from.left)};
    
    double translated_top{static_cast<double>(from.top)};
    double translated_left{static_cast<double>(from.left)};
    
    double translated_bottom{static_cast<double>(from.bottom)};
    double translated_right{static_cast<double>(from.right)};
    
    double dx{Dx/max(abs(Dx),abs(Dy))};
    double dy{Dy/max(abs(Dx),abs(Dy))};
    
    
    bool collided{false};
    for (int i{0}; i < max(abs(Dx),abs(Dy)) && !collided; ++i)
    {
        
        translated_top += dy;
        translated_bottom += dy;
        translated_left += dx;
        translated_right += dx;
        
        
        from.top = ceil(translated_top);
        from.bottom = ceil(translated_bottom);
        from.right = ceil(translated_right);
        from.left = ceil(translated_left);
        
    
        collided = intersect(from); 
    }
        
    if (collided) 
    {    
        collision_point.x = from.left;
        collision_point.y = from.top;
    }
    return collided;
}
