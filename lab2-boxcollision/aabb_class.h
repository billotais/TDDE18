#ifndef AABB_H
#define AABB_H

#include <iostream>
#include <iomanip>
#include <utility>
#include <algorithm>
#include <cmath>
#include <functional>
#include "Point.h"

class AABB
{
    public :
        AABB(int,int,int,int);
        AABB(Point const&,Point const&);
        AABB(const AABB&); //copy ctor
        bool contain(int,int) const;
        bool contain(Point const&) const;
        bool intersect(AABB const&) const;
        AABB min_bounding_box(AABB const&) const;
        bool may_collide(AABB const&,AABB const&) const;
        bool collision_point(AABB,AABB const&, Point&);
        
        ~AABB();
    private :
        AABB(); //empty ctor
        int top; //y axis
        int left; //x axis
        int bottom; //y axis
        int right;//x axis
};

#endif