#ifndef POINT_H
#define POINT_H

// 7-3.
// Comment: If getter and setter exist why not private members?
class Point
{
        
    public :
        Point();
        Point(int,int);
        int x;
        int y;
};

#endif