---
title : "Cheat Sheet - C++"
author : Loïs Bilat
date : \today
...

# IO

We include `<iostream>`

Print to console

```c++
cout << "Text to print" << x << "x is any variable" << endl;
cerr << "For error message" << endl;
```

Text format 
```c++
cout << fixed; // Always show decimals
cout << setprecision(2); // How many decimals
cout << setw(12) << my_string << endl; // What is printed will take 12 of space.
cout << setfill('*'); // Set the filling character
```

Input file stream

```c++
ifstream file_stream{"fichier.txt"};
// OR
ifstream file_stream{};
file_stream.open("fichier.txt");

file_stream >> x; // Read from file
getline(file_stream, my_string);

...

file_stream.close();
```
Output file stream

```c++
ofstream file_stream{"fichier.txt"};
// OR
ofstream file_stream{};
file_stream.open("fichier.txt");

file_stream << "text"; // write into file

...

file_stream.close();
```

Output/Input string stream
```c++
istringstream iss{"string"};
// Or
istringstream iss{};
iss.str("string");
iss >> s;
iss.close()

ostringstream oss{};

oss << "text";
string str{oss.str()}
oss.close()
```
Errors stream
```c++
bool eof()  // end of file
bool fail() // logical error
bool bad()  // serious error
bool good() // no error
```
Error checking
```c++
int x{};
do
{
    if (!cin >> x)
    {
        // Something bad
        if (cin.bad()) throw ios::failure("bad input");
        if (cin.eof()) throw ios::failure("eof");
        if (cin.fail())
        {
            cout << "logical error" << endl;
            cin.clear() // remove errors
            cin.ignore(1024,'\n'); // ignore line
        }
    }
} while (x != correct_input);
```

# Type casting

`static_cast<new type>(input)` will cast a value

# Structs/const/references

Structures
```c++
    struct Person
    {
        string nom;
        int age;
    }
    Person p{"Loïs", 20};
    age = 21;
```
Const
```c++
    Person const p{"Loïs", 20};
    age = 21; // Error
```
Reference
```c++
    int age{20};
    int & alias_age{age};
    alias_age = 30 // => age = 30;
```
Reference const
```c++
    int age{20};
    int const & alias_age{age};
    alias_age = 30 // error
```
