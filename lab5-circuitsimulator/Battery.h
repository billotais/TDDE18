#ifndef BATTERY_H
#define BATTERY_H

#include "Component.h"
#include <string>

class Battery : public Component {
    public:
        Battery(Connexion*, Connexion*, double, std::string);
        void update(double);

        double current() const;
        ~Battery();
    private:
        double const battery_voltage;
};
#endif
