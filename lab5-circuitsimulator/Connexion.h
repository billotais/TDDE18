#ifndef CONNEXION_H
#define CONNEXION_H


class Connexion {
    public:
        Connexion();
        double voltage() const;
        void voltage(double);
        ~Connexion();

    private:
        double m_voltage;
};

#endif
