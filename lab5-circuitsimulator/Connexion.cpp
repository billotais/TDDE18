#include "Connexion.h"

Connexion::Connexion() : m_voltage{0.0}
{
}

double Connexion::voltage() const
{
    return m_voltage;
}

void Connexion::voltage(double v)
{
    m_voltage = v;
}

Connexion::~Connexion()
{
}
