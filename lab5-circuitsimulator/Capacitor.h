#ifndef CAPACITOR_H
#define CAPACITOR_H

#include "Component.h"
#include <string>
#include <cmath>

class Capacitor : public Component {
    public:
        Capacitor(Connexion*, Connexion*, double, std::string);
        void update(double);

        double current() const;
        ~Capacitor();
    private:
        double const capacity;
        double stored;
};
#endif
