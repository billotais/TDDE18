#include "Capacitor.h"
#include <iostream>

using namespace std;


Capacitor::Capacitor(Connexion* left_, Connexion* right_, double capacity_, string name_)
    : Component(left_, right_, name_), capacity{capacity_}, stored{0}
{
}

void Capacitor::update(double time_step)
{
    double l_v{left->voltage()};
    double r_v{right->voltage()};
    double to_move{capacity * (abs(l_v - r_v) - stored) * time_step};
    stored += to_move;
    if (l_v < r_v)
    {
        left->voltage(l_v + to_move);
        right->voltage(r_v - to_move);
    }
    if (l_v > r_v)
    {
        left->voltage(l_v - to_move);
        right->voltage(r_v + to_move);
    }
}

double Capacitor::current() const
{
    return capacity*(voltage()-stored);
}

Capacitor::~Capacitor()
{
}
