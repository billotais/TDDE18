#ifndef NETWORK_H
#define NETWORK_H

#include <vector>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "Connexion.h"
#include "Component.h"

class Network {
    public:
        Network();
        void add_component(Component*);
        void add_connexion(Connexion*);
        void simulate(int, int, double) const;
        ~Network();

    private:
        std::vector<Component*> components{};
        std::vector<Connexion*> connexions{};
};

#endif
