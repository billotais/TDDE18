#include "Battery.h"

using namespace std;

Battery::Battery(Connexion* left_, Connexion* right_, double voltage, string name_)
    : Component(left_, right_, name_), battery_voltage{voltage}
{
}

void Battery::update(double time_step)
{
    // We use time_step even if it's not needed
    //to remove the warning "variable not used"

    left->voltage(time_step*0);
    right->voltage(battery_voltage);
}

double Battery::current() const
{
    return 0; //no current in battery if we follow the instructions model
}

Battery::~Battery()
{
}
