#include "Component.h"

using namespace std;

Component::Component(Connexion* left_, Connexion* right_, string name_)
    : left{left_}, right{right_}, comp_name{name_}
{
}

double Component::voltage() const
{
    return abs(right->voltage() - left->voltage());
}

string Component::name() const
{
    return comp_name;
}

Component::~Component()
{
}
