#include "Resistor.h"


using namespace std;


Resistor::Resistor(Connexion* left_, Connexion* right_, double resistance_, string name_)
    : Component(left_, right_, name_), resistance{resistance_}
{
}

void Resistor::update(double time_step)
{
    double l_v{left->voltage()};
    double r_v{right->voltage()};
    double to_move{abs(l_v - r_v)/resistance*time_step};
    if (l_v < r_v)
    {
        left->voltage(l_v + to_move);
        right->voltage(r_v - to_move);
    }
    if (l_v > r_v)
    {
        left->voltage(l_v - to_move);
        right->voltage(r_v + to_move);
    }

}

double Resistor::current() const
{
    return voltage() / resistance;
}

Resistor::~Resistor()
{
}
