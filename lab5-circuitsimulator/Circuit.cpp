/*Comments:

TO DO:
6-3 DONE

7-1 DONE

8-8 Almost everywhere in your code you have taken into consideration that you
	don't know which of the two terminals that has the biggest value (charge).
	Do that for ALL places where it's relevant.
  // Not sure to understand what is the problem. I added an abs() in Component::voltage(). Everything else
  // is done considering we don't know what is the higher terminal. The execption is Battery, but we need to define
  // a direction dor thr battery and keep it.

8-8 There's a mix-up in the battery constructor which results in the fact that
	charge may be on the wrong terminal. DONE

8-8 Print all the components' voltage and current after all of them have been
	updated. This will make your printout as in the assignment paper. DONE

8-8 Member variables that are not changing and functions which don't change the
	object should be declared const. DONE
*/

#include <cstdlib> //for the exit macro
#include <iostream>
#include "Network.h"
#include "Battery.h"
#include "Resistor.h"
#include "Capacitor.h"

void show_usage(char* program_name)
{
    std::cout << "Circuit Simulator : Help :" << std::endl;
    std::cout << "  Usage : program_name nb_iterations nb_print, time_step, battery_voltage"<< std::endl;
    std::cout << "  Example : " << program_name << " 200000 10 0.01 24.0"<< std::endl;
}

void circuit_1(int n_iterations, int n_print, double time_step, double battery_voltage)
{
    Network n{};
    Connexion P, N, R124, R23;
    n.add_connexion(&P);
    n.add_connexion(&N);
    n.add_connexion(&R124);
    n.add_connexion(&R23);

    //Battery b{&N, &P, 24.0, "Bat"};
    Battery b{&N, &P, battery_voltage, "Bat"};
    Resistor r1{&P, &R124, 6.0, "R1"};
    Resistor r2{&R124, &R23, 4.0, "R2"};
    Resistor r3{&R23, &N, 8.0, "R3"};
    Resistor r4{&R124, &N, 12.0, "R4"};

    n.add_component(&b);
    n.add_component(&r1);
    n.add_component(&r2);
    n.add_component(&r3);
    n.add_component(&r4);

    //n.simulate(200000,10,0.01);
    n.simulate(n_iterations,n_print,time_step);
}

void circuit_2(int n_iterations, int n_print, double time_step, double battery_voltage)
{
    Network n{};
    Connexion P, N, L, R;
    n.add_connexion(&P);
    n.add_connexion(&N);
    n.add_connexion(&L);
    n.add_connexion(&R);

    Battery b{&N, &P, battery_voltage, "Bat"};
    Resistor r1{&P, &L, 150.0, "R1"};
    Resistor r2{&P, &R, 50.0, "R2"};
    Resistor r3{&L, &R, 100.0, "R3"};
    Resistor r4{&L, &N, 300.0, "R4"};
    Resistor r5{&R, &N, 250.0, "R5"};

    n.add_component(&b);
    n.add_component(&r1);
    n.add_component(&r2);
    n.add_component(&r3);
    n.add_component(&r4);
    n.add_component(&r5);

    n.simulate(n_iterations,n_print,time_step);
}

void circuit_3(int n_iterations, int n_print, double time_step, double battery_voltage)
{
    Network n{};
    Connexion P, N, L, R;
    n.add_connexion(&P);
    n.add_connexion(&N);
    n.add_connexion(&L);
    n.add_connexion(&R);

    Battery b{&N, &P, battery_voltage, "Bat"};
    Resistor r1{&P, &L, 150.0, "R1"};
    Resistor r2{&P, &R, 50.0, "R2"};
    Capacitor c3{&L, &R, 1.0, "C3"};
    Resistor r4{&L, &N, 300.0, "R4"};
    Capacitor c5{&R, &N, 0.75, "C5"};

    n.add_component(&b);
    n.add_component(&r1);
    n.add_component(&r2);
    n.add_component(&c3);
    n.add_component(&r4);
    n.add_component(&c5);

    n.simulate(n_iterations,n_print,time_step);
}

int main(int argc, char *argv[])
{
    if(argc !=5)
    {
        show_usage(argv[0]);
        return EXIT_SUCCESS;
    }

    int n_iterations{};
    int n_print{};
    double time_step{};
    double battery_voltage{};
    try
    {
        n_iterations=std::stoi(argv[1]);
        n_print=std::stoi(argv[2]);
        time_step=std::stod(argv[3]);
        battery_voltage=std::stod(argv[4]);
    }
    catch(...)
    {
        std::cerr << "One of the parameter is Wrong"<< std::endl;
        show_usage(argv[0]);
        return EXIT_FAILURE;
    }

    circuit_1(n_iterations, n_print, time_step, battery_voltage);
    circuit_2(n_iterations, n_print, time_step, battery_voltage);
    circuit_3(n_iterations, n_print, time_step, battery_voltage);


    return 0;
}
