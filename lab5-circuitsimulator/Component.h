#ifndef COMPONENT_H
#define COMPONENT_H

#include "Connexion.h"
#include <string>
#include <cmath>


class Component {
    public:
        Component(Connexion*, Connexion*, std::string);
        virtual void update(double) = 0;
        double voltage() const;
        virtual double current() const = 0;
        std::string name() const;
        virtual ~Component() = 0;
    protected:
        Connexion* const left;
        Connexion* const right;
        std::string const comp_name;
};

#endif
