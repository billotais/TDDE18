#include "Network.h"
using namespace std;

Network::Network()
{

}
void Network::add_component(Component* c)
{
    components.push_back(c);
}
void Network::add_connexion(Connexion* c)
{
    connexions.push_back(c);
}
void Network::simulate(int n_iterations, int n_print, double time_step) const
{
    cout << fixed;

    for (Component* c : components)
    {
        cout << setw(12);
        cout << c->name();
    }
    cout << endl;

    /////


    for (unsigned int i{0}; i < components.size(); ++i)
    {
        cout << setw(6);
        cout << "Volt";
        cout << setw(6);
        cout << "Curr";
    }
    cout << endl;

    //////

    cout << fixed;
    cout << setprecision(2);

    int i{0};
    while(i < n_iterations)
    {
        for (Component* c : components)
        {
            c->update(time_step);
        }
        if ((i % (n_iterations/n_print) == 0 && i) || i == (n_iterations - 1))
        {
            for (Component* c : components)
            {
                cout << setw(6);
                cout << c->voltage();
                cout << setw(6);
                cout << c->current();
            }
            cout << endl;
        }
        ++i;
    }
}
Network::~Network()
{

}
