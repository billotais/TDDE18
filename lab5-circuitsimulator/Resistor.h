#ifndef RESISTOR_H
#define RESISTOR_H

#include "Component.h"
#include <string>
#include <cmath>

class Resistor : public Component {
    public:
        Resistor(Connexion*, Connexion*, double, std::string);
        void update(double);

        double current() const;
        ~Resistor();
    private:
        double const resistance;


};
#endif
