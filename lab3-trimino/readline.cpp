#include "readline.h"
using namespace std;


void read_line(string const& text, string const& file, int min, int max, int line) {
    istringstream iss(text);

    int number_1{0};
    int number_2{0};
    int number_3{0};
    string image_name{};
    bool found_image{false};
    bool valid_syntax{true};
    bool valid_args{true};
    cout << "------------------------------------------------" << endl;
    cout << "Line n° " << line << " of file \"" << file << "\" : \"" << text << "\"" << endl;

    // Check if the numbers are here
    if (!(iss >> number_1)) {
        cerr << "   3 numbers are missing" << endl;
        valid_syntax = false;
    }
    else if (!(iss >> number_2)) {
        cerr << "   2 numbers are missing" << endl;
        valid_syntax = false;
    }
    else if (!(iss >> number_3)) {
        cerr << "   1 number is missing" << endl;
        valid_syntax = false;
    }
    // If all the numbers are here
    if (valid_syntax) {
        // Check if they are in the bounds
        if (number_1 < min || max < number_1) {
            cerr << "   The 1st number is out of range" << endl;
            valid_args = false;
        }
        if (number_2 < min || max < number_2) {
            cerr << "   The 2nd number is out of range" << endl;
            valid_args = false;
        }
        if (number_3 < min || max < number_3) {
            cerr << "   The 3rd number is out of range" << endl;
            valid_args = false;
        }
        // Check if the order is correct
        if (!((number_1 <= number_2 && number_2 <= number_3) || (number_2 <= number_3 && number_3 <= number_1) || (number_3 <= number_1 && number_1 <= number_2))) {
            cerr << "   The order of the numbers is not correct" << endl;
            valid_args = false;
        }

        // Check if there is an image
        if (iss >> image_name) {
            found_image = true;
            cout << "   The image is : \"" << image_name << "\"" << endl;
        }
        else {
            cout << "   There is no image" << endl;
        }
        // If everything is valid, print cleaned line
        if (valid_args) {
            cout << "   This is a valid line : \"" << number_1 << " " << number_2 << " " << number_3  << (found_image ? (" " + image_name) : "") << "\"" << endl;
        }
        // Other wise error message
        else {
            cerr << "   => The arguments are not valid" << endl;
        }
    }
    // If the format is not correct, error message
    else {
        cerr << "   => The format of the line is not valid" << endl; 
    }
    cout << "------------------------------------------------" << endl;   
}

