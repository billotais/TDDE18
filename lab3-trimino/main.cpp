#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <algorithm>
#include <memory>

#include <cstdlib> // for the macro exit faulure and success

#include "readline.h"

/* Complementary work needed
 * 1-3
 * Avoid using pointers when there is no need
 *
 */

using namespace std;

int get_bound_from_input(const string &bond_name)
{
    int inputint{-1};
    const int lim_max{numeric_limits<int>::max()/2};
    do
    {
        cout << "Please enter the " << bond_name << " bound : ";
        
        if( !(cin >> inputint) ) // If the input is not correct, set an error or fail 
        {
            if (cin.bad() || cin.eof()) 
            {
                throw ios::failure("Error bad stream");
            }
            else if (cin.fail()) 
            {
                cout << "in FAIL error"<< endl;
                
                cin.clear();

                cin.ignore(1024, '\n');
                inputint = -1;
            }
        }  
        if (inputint < 0) cerr << "ERROR: The bound must be at least 0" << endl;
        
        else if (inputint >= lim_max) cerr << "ERROR: The bound must be at most " << lim_max << endl;
    }
    while (inputint < 0 or inputint >= lim_max);
    cin.ignore(1024, '\n');
    return inputint;
}

bool open_file(const string &path,ifstream& file_Stream)
{
    file_Stream.open(path);
    
    if(!(file_Stream))
    {   

        cerr << "Error: " << path << " may not exist." << endl;
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    cout << endl;
    filebuf dummy_buffer{};
    istream stream_reader{&dummy_buffer};
    ifstream file_Stream{};

    bool using_file{false};
    int lower_bound{};
    int upper_bound{};
    if( argc == 4 or argc == 2 ) // then we have "file bound1 bound2" or "file" given in args
    {
        if( !open_file(argv[1],file_Stream) ) // if the file is not correctly openned we exit
            return EXIT_FAILURE;

        stream_reader.rdbuf(file_Stream.rdbuf());//&file_Stream; //if we are here then the file is correctly opnned and we move the file stream to our input stream
        using_file = true;
        if( argc == 2 ) // if we have only two args then only the filename and the program should be in our args (if not then we failed to open the file and therefore exited before being here)
        {
            // if we have the filename only then we need the bound.So we get them here
            lower_bound = get_bound_from_input("first");
            upper_bound = get_bound_from_input("second");
        }
        else
        {
            lower_bound=stoi(argv[2]);
            upper_bound=stoi(argv[3]);
        }

        if( lower_bound > upper_bound ) //make some cohesion
        {
            swap(lower_bound,upper_bound);
        }
    }
    else if ( argc == 1 ) //here we are in the case where the progam was launched without args
    {
        //first lets say to the user the usage in case he don't know them
        cout << "Note : Usage: " << argv[0] << " inputfile.txt lowerbound upperbound or " << endl << argv[0] << " inputfile.txt" << endl;

        //then lets ask him what mode he want
        char file_or_interactive{};
        do
        {
            cout << "No filename given do you want to use the interactive mode ? Or do you Want to input the filename ? [i(interactive)/f(filename)] :";
            if( !(cin >> file_or_interactive) ) 
            {
                if (cin.bad() || cin.eof()) return EXIT_FAILURE;
                else if (cin.fail()) 
                {
                    cin.ignore(1024, ' ');
                    cin.clear();
                }
            }
            // Check error TODO
        }while( file_or_interactive != 'i' and file_or_interactive != 'f' );
        cin.ignore(256, '\n');

        //we ask the user about the bound
        cout << "Please setup the bound" << endl;
        lower_bound = get_bound_from_input("first");
        upper_bound = get_bound_from_input("second");

        if( lower_bound > upper_bound ) swap(lower_bound,upper_bound); // Make some cohesion
        
        if( file_or_interactive == 'f' )
        {
            string path{};
            do
            {
                cout << "Please enter a valid filename :";   
                getline(cin, path);
            }

            while( !open_file(path,file_Stream) );
            stream_reader.rdbuf(file_Stream.rdbuf());//if we are here then the file is correctly opnned and we move the file stream to our input stream
            using_file = true;
        }
        else
        {
            stream_reader.rdbuf(cin.rdbuf());
            cout << "Interactive mode : you can now input the data :" << endl;
        }
    }
    else
    {
        cerr << "Usage: " << argv[0] << " inputfile.txt lowerbound upperbound or " << endl;
        cerr << argv[0] << " inputfile.txt" << endl;
        return EXIT_FAILURE;
    }

    // reading the file line by line , or getting the user input line by line
    string current_line{};
    int line_count{0};

    while(getline( stream_reader, current_line )) 
    {
        read_line(current_line,"file",lower_bound,upper_bound,line_count);
        ++line_count;
    }
   
    int return_code{EXIT_SUCCESS};
    if ( stream_reader.eof() )
    {
        cerr << "End of file." << endl;
    }
    else if ( stream_reader.bad() ) 
    {
        cerr << "Error while reading file (Bad error)" << endl;
        return_code = EXIT_FAILURE;
    }
    else if ( stream_reader.fail() )
    {
         cerr << "Logic error." << endl;
         return_code = EXIT_FAILURE;
    }
    if (using_file) 
        file_Stream.close();
    
    return return_code;
}
