#ifndef READLINE_H
#define READLINE_H

#include <iostream>
#include <sstream>
#include <fstream>

void read_line(std::string const& text, std::string const& file, int min, int max, int line);

#endif
